package com.rilixtech.androidutil;

import android.app.KeyguardManager;
import android.content.Context;
import android.os.Build;
import android.os.PowerManager;
import android.util.Log;

public class ScreenUtil {

  private static final String TAG = ScreenUtil.class.getSimpleName();
  /**
   * Returns true if the device is locked or screen turned off (in case password not set)
   */
  public static boolean isDeviceLocked(Context context) {
    boolean isLocked;

    // First we check the locked state
    KeyguardManager km = (KeyguardManager) context.getSystemService(Context.KEYGUARD_SERVICE);
    boolean inKeyguardRestrictedInputMode;

    if(km == null) {
      inKeyguardRestrictedInputMode = false;
    } else {
      if (Build.VERSION.SDK_INT >= 28) {
        inKeyguardRestrictedInputMode = km.isDeviceLocked();
      } else {
        inKeyguardRestrictedInputMode = km.inKeyguardRestrictedInputMode();
      }
    }

    if (inKeyguardRestrictedInputMode) {
      isLocked = true;
    } else {
      // If password is not set in the settings, the inKeyguardRestrictedInputMode() returns false,
      // so we need to check if screen on for this case
      PowerManager powerManager = (PowerManager) context.getSystemService(Context.POWER_SERVICE);

      if(powerManager == null) {
        isLocked = true;
      } else {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT_WATCH) {
          isLocked = !powerManager.isInteractive();
        } else {
          isLocked = !powerManager.isScreenOn();
        }
      }
    }

    Log.d(TAG, "Screen is locked = " + isLocked);
    return isLocked;
  }
}
