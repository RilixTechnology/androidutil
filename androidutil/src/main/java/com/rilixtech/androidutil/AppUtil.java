package com.rilixtech.androidutil;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;

public class AppUtil {

  /**
   * Launch an application from another application on Android
   * @param context caller context
   * @param packageName package name of the app to be launched.
   */
  public static void startNewActivity(Context context, String packageName) {
    Intent intent = context.getPackageManager().getLaunchIntentForPackage(packageName);
    if (intent == null) {
      // Bring user to the market or let them choose an app?
      intent = new Intent(Intent.ACTION_VIEW);
      intent.setData(Uri.parse("market://details?id=" + packageName));
    }
    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
    context.startActivity(intent);
  }
}
